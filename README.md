# Dynamic cellular Potts model (DyCPM)

DyCPM is a demonstration of the [Poissonian Cellular-Potts models][1], which simulates a sorting process taking place
in [isolated inner cellular mass from mouse embryo][2].

The package code is provided as a python package `dycpm` (**Dy**namic **C**ellular **P**otts **M**odel) under CC BY 4.0 License.

## Installation

The DyCPM package requires a working installation of [Rust compiler](https://www.rust-lang.org/tools/install) and [Python](https://wiki.python.org/moin/BeginnersGuide/Download).
Then, to install the package, run `pip install .` in a command line from the project root.

## Unit tests

To run unit-test coverage of the Rust code run `cargo test` in the command line. To test python bindings, install [PyTest](https://docs.pytest.org/en/7.1.x/getting-started.html) and run it as `pytest` in the command line from the project root.

## References

Please consider referencing

[1] R. Belousov et al. (2024), When time matters: Poissonian cellular Potts models reveal nonequilibrium kinetics of cell sorting.
(accepted to publication in PRL)<br/>
https://doi.org/10.48550/arXiv.2306.04443

[2] Moghe et al. (2023). Apical-driven cell sorting optimised for tissue geometry ensures robust patterning.<br/>
https://doi.org/10.1101/2023.05.16.540918

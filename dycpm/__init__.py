"""User-level bindings for DyCPM

Attributes:
    MEDIUM (int): Extra-cellular matrix type of medium
    ECM (int): Extra-cellular matrix type of medium
    EPI (int): Epi type of cells
    PRE (int): PrE type of cells
    DIVISION_VOLUME_DISTRIBUTION (list[int]): A sample of mother cells' volumes at the division time,
        which defines an empirical distribution
    TENSION_CONSTANTS (list[float]): Empirical tension parameters
    AVERAGE_TENSION (list[float]): Average empirical tension parameters
"""
import json, bson
import numpy as np
from .dycpm import MEDIUM, ECM, EPI, PRE
from .dycpm import Kernel

DIVISION_VOLUME_DISTRIBUTION = [
    2406.63, 2428.09, 2455.23, 2517.92, 2994.28, 3002.65, 3110.31, 3116.73,
    3294.93, 4133.67
]
# Interfaces characterized in the following order: MEDIUM/EPI, MEDIUM/PRE, EPI/EPI, EPI/PRE, PRE/PRE
TENSION_CONSTANTS = [1.14e5, 0.85e5, 1.47e5, 1.63e5, 1.33e5]
# Intercace tension constants characterized in the following order: MEDIUM/EPI, MEDIUM/PRE, EPI/EPI, EPI/PRE, PRE/PRE
AVERAGE_TENSION = [1.0e5, 1.0e5, 1.4e5, 1.4e5, 1.4e5]

def sample_division_volume():
    """Sample a value from the empirical distribution defined by `DIVISION_VOLUME_DISTRIBUTION`"""
    target = np.random.random()
    sz = len(DIVISION_VOLUME_DISTRIBUTION)

    probabilities = [(i + 1) / sz for i in range(sz)]
    base = DIVISION_VOLUME_DISTRIBUTION[0]
    last = 0
    for i in range(sz):
        now = probabilities[i]
        if target < now: return round(
            base if i == 0 else base + (target - last) * (
                DIVISION_VOLUME_DISTRIBUTION[i] - base
            ) / (now - last)
        )
        last = now
        base = DIVISION_VOLUME_DISTRIBUTION[i]

    return round(DIVISION_VOLUME_DISTRIBUTION[-1])

class Cell:
    """Description of a cell state

    Attributes:
        type_id (int): Cell type (see `dycpm.MEDIUM`, `dycpm.ECM`, `dycpm.EPI`, or `dycmp.PRE`)
        parent_id (int): ID of the mother cell
        age (int): Current cell age as the number of simulation steps
        preferred_volume (float): Preferred volume
        division_volume (float): Division volume
        voxel_ids (list[int]): IDs of occupied voxels
    """
    def __init__(self, **kwargs):
        """Initialize cell attributes

        Keyword arguments:
            type_id (int): Required cell type id (`dycpm.EPI` or `dycmp.PRE`)
            parent_id (int): Optional ID of the mother cell (zero by default)
            age (int): Optional current cell age (zero by default)
            preferred_volume (float): Required preferred volume
            division_volume (float): Required division volume
            voxel_ids (list[int]): Optional IDs of occupied voxels (`[]` by default)
        """
        self.type_id = kwargs.pop('type_id')
        self.parent_id = kwargs.pop('parent_id', 0)
        self.age = kwargs.pop('age', 0)
        self.preferred_volume = kwargs.pop('preferred_volume')
        self.division_volume = kwargs.pop('division_volume')
        self.voxel_ids = kwargs.pop('voxel_ids', [])
        if len(kwargs) > 0:
            raise TypeError(f'Unknown kwargs: {list(kwargs.keys())}')


class State:
    """Represents a simulation state

    Attributes:
        side (int): Size of the simulation-box side as a number of voxels
        time (int): Time of simulation as a number of steps
        cells (dict[int, Cell]): Collection of cells
        ecm (list[int]): Collection of voxels occupied by ECM
        ecm_parameters (list[float]): Paremeters that characterize ECM in the following order:
            [0] The energy penalty per volume of ECM;
            [1] Active exponent of transitions from `MEDIUM` to `ECM`;
            [2] Active exponent of transitions from `EPI` to `ECM`;
            [3] Active exponent of transitions from `PRE` to `ECM`;
            [4] Active exponent of transitions from `ECM` to `ECM`.
        action_probabilities (list[float]): Action rates converted to probabilities for medium, EPI, and PrE
        death_probability (float): Probability of cell death
        growth_rate (list[float]): Rate of cell growth
        division_distribution (list[float]): Empirical distribution of cell volume at division time
        volume_elasticity (list[float]): Cell volume elasticity for EPI and PrE cells respectively
        active_exponents (list[float]): Active exponents for the following transitions:
            [0] medium to EPI;
            [1] medium to PrE;
            [2] EPI to medium;
            [3] EPI -> EPI;
            [4] EPI -> PrE;
            [5] PrE -> medium;
            [6] PrE -> EPI;
            [7] PrE -> PrE.
        surface_tension (list[float]): Surface tension between voxel types in the order medium/EPI, medium/PrE, EPI/EPI,
            EPI/PrE, PrE/PRE
        rng (dict|None): State of the random-number generator may be absent
    """
    def __init__(self, **kwargs):
        """Initialize state

        Keyword arguments:
            side (int): Required size of the simulation-box
            time (int): Optional time counter (zero by default)
            cells (dict[int,dict|Cell]): Optional collection of cells (zero by defaul)
            ecm (list[int]): Optional collection of voxels occupied by ECM (empty by default)
            ecm_parameters (list[float]): Optional parameters of the ECM (no production of ECM by default)
            action_probabilities (list[int]): Required action rates converted to probabilities
            growth_rate (float): Optional rate of cell growth (zero by default)
            division_distribution (list[float]): Optional empirical distribution of cell volume
                (`dycpm.DIVISION_VOLUME_DISTRIBUTION` by default)
            volume_elasticity (list[float]): Required volume elasticity of cell types
            active_exponents (list[float]): Optional active exponents
            surface_tension (list[float]): Required surface tension between voxel types
            rng (dict): Optional state of the random-number generator
        """
        self.side = kwargs.pop('side')
        self.time = kwargs.pop('time', 0)
        self.cells = {
            int(k): (Cell(**v) if isinstance(v, dict) else v)
            for k, v in kwargs.pop('cells', {}).items()
        }
        self.ecm = kwargs.pop('ecm', [])
        self.ecm_parameters = kwargs.pop('ecm_parameters', [1.7976931348623157e+308] + 4 * [float('-inf')])
        self.action_probabilities = kwargs.pop('action_probabilities')
        self.death_probability = kwargs.pop('death_probability', 0.0)
        self.growth_rate = kwargs.pop('growth_rate', 0)
        if not hasattr(self.growth_rate, '__len__'): self.growth_rate = 2 * [self.growth_rate]
        self.division_distribution = kwargs.pop(
            'division_distribution', DIVISION_VOLUME_DISTRIBUTION.copy()
        )
        self.volume_elasticity = kwargs.pop('volume_elasticity')
        self.active_exponents = kwargs.pop('active_exponents', 8 * [0.0])
        self.surface_tension = kwargs.pop('surface_tension')
        self.modulation = kwargs.pop('modulation', [1.0, 1.0, 1.0])
        self.bead = kwargs.pop('bead', [])
        self.bead_parameters = kwargs.pop('bead_parameters', 3 * [float('nan')])
        if 'rng' in kwargs: self.rng = kwargs.pop('rng')
        if len(kwargs) > 0:
            raise TypeError(f'Unknown kwargs: {list(kwargs.keys())}')

    def __getattr__(self, name):
        if name == 'rng': return None
        raise AttributeError(f'State has no attribue `{name}`')

    def to_json(self):
        """Export data as JSON-serializable data

        Returns:
            __dict__: JSON-serializable data
        """
        json_data = self.__dict__.copy()
        json_data['cells'] = json_data['cells'].copy()
        for key, value in json_data['cells'].items():
            json_data['cells'][key] = value.__dict__
        return json_data

    def to_json_file(self, file):
        """Save data to a JSON file

        Args:
            file (str): Path to the exported file
        """
        with open(file, 'w') as json_file:
            json.dump(self.to_json(), json_file)

    @classmethod
    def from_json_file(cls, file):
        """Load data from a JSON file

        Args:
            file (str): Path to the file to load
        """
        with open(file, 'r') as json_file: return cls(**json.load(json_file))

    def to_bson_file(self, file):
        """Save data to a BSON file

        Args:
            file (str): Path to the exported file
        """
        data = self.__dict__.copy()
        data['cells'] = data['cells'].copy()
        for key, value in data['cells'].items():
            data['cells'][key] = value.__dict__

        with open(file, 'wb') as bson_file:
            bson_file.write(bson.dumps(data))

    @classmethod
    def from_bson_file(cls, file):
        """Load data from a BSON file

        Args:
            file (str): Path to the file to load
        """
        with open(file, 'rb') as bson_file: return cls(**bson.loads(bson_file.read()))

    def voxel_id(self, voxel_coordinates):
        """Convert 3D index of a voxel into its ID in the simulation lattice

        Args:
            voxel_coordinates (list[int]): 3D index of the voxel

        Returns:
            int: ID of the voxel in the simulation lattice
        """
        return voxel_coordinates[0] + voxel_coordinates[1] * self.side + voxel_coordinates[2] * self.side**2

    def voxel_coordinates(self, voxel_id):
        """Get 3D index of a voxel specicified by its ID in the simulation lattice

        Returns:
            list[int]: 3D index of the voxel
        """
        factor = self.side**2
        rem = voxel_id % factor
        return [rem % self.side, rem // self.side, voxel_id // factor]

    def value_type(self, cell_id):
        """Return cell type

        Returns:
            int: One of the values `dycpm.MEDIUM`, `dycpm.ECM`, `dycpm.EPI`, `dycpm.PRE`
        """
        if cell_id <= 0: return cell_id
        return self.cells[cell_id].type_id

    def cell_positions(self):
        """Return cell positions
        
        Cell position is defined as the geometric center of the voxel it occupies.

        Return:
            __dict__[int, list[float]]: A map between cell IDs and coordinates of their positions
        """
        return {
            cell_id: np.mean([
                self.voxel_coordinates(voxel)
                for voxel in cell.voxel_ids
            ], axis = 0) for cell_id, cell in self.cells.items()
        }

    def cell_fates(self):
        """Return cell fates

        Fate is synonymous with the cell type—one of the values `dycpm.MEDIUM`, `dycpm.ECM`, `dycpm.EPI`, `dycpm.PRE`.

        Returns:
            __dict__[int, int]: A map between cell IDs and their fates
        """
        return {cell_id: cell.type_id for cell_id, cell in self.cells.items()}

    def cell_gyration_tensors(self):
        """Return gyration tensors for each cell

        Returns:
            __dict__[int, list[float]]: A map between cell IDs and their gyration tensors
        """
        return {
            cell_id: np.cov(
                np.array([self.voxel_coordinates(voxel) for voxel in cell.voxel_ids]).T
            ) for cell_id, cell in self.cells.items()
        }

    def cell_principle_values(self):
        """Return square roots of the eigenvalues of the gyration vector in the descending order

        Returns:
            __dict__[int, list[float]]: A map between cell IDs and the principle values of their gyration tensors
        """
        gts = self.cell_gyration_tensors()
        return {
            cell_id: np.sort(np.sqrt(np.linalg.eigvals(gt)))[::-1]
            for cell_id, gt in gts.items()
        }

    def cell_volumes(self):
        """Return cell volumes

        Returns:
            __dict__[int, int]: A map between cell IDS and the number of voxels they occupy
        """
        return {
            cell_id: len(cell.voxel_ids) for cell_id, cell in self.cells.items()
        }

    def special_coordinates(self, voxel_type = "ecm"):
        """Return coordinates of special voxels (experimental)"""
        if voxel_type not in ["ecm", "bead"]: raise ValueError(f'Unknown special-voxel type: {voxel_type}')
        return [self.voxel_coordinates(voxel) for voxel in self.__dict__[voxel_type]]

    def volume(self, exclude_ecm = False):
        """Return the total volume of all cells and ECM (experimental)"""
        ecm_volume = 0.0 if exclude_ecm else len(self.ecm)
        return np.sum([len(cell.voxel_ids) for cell in self.cells.values()]) + ecm_volume

    def com(self):
        """Return geometric center of cell positions

        Similar to `State.cell_positions()`, this method returns cell coordinates in the geometric-center reference frame.
        
        Returns:
            __dict__[int, list[float]]: A map between cell IDs and coordinates of their positions
        """
        xyz = [
            np.mean([
                self.voxel_coordinates(voxel)
                for voxel in cell.voxel_ids
            ], axis = 0) for cell in self.cells.values()
        ]
        return np.mean(xyz, axis = 0)

    def lattice(self):
        """Return values of the CPM lattice (low-level access to lattice values)"""
        values = np.zeros((self.side, ) * 3)
        view = values.transpose()
        for cell_id, cell in self.cells.items():
            view.flat[cell.voxel_ids] = cell_id
        view.flat[self.ecm] = dycpm.ECM
        return values

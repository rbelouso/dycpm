"""Tools for analysis of CPM states"""
import json, itertools
import numpy as np, networkx as nx
from . import State
from . import ECM, PRE

def sorting_score(state):
    """Calculate the sorting score of a state

    Args:
        state (dycpm.State): the state to analyze

    Returns:
        float: Sorting score of the state
    """
    if len(state.cells) == 0: return np.nan
    fates = np.asarray(list(state.cell_fates().values()), dtype = int)
    epi = fates == 1
    pre = fates == 2

    pos = np.asarray(list(state.cell_positions().values()))
    pos -= np.mean(pos, axis = 0)
    rcom = np.linalg.norm(pos, axis = 1)
    scores = np.sign([pair[0] - pair[1] for pair in itertools.product(rcom[pre], rcom[epi])])

    return np.mean(scores) if len(scores) > 0 else np.nan

def neighbor_index(state, same, other, glue = []):
    """Calculate the neighbor index for a given cell value

    Args:
        state (dycpm.State): the state to analyze
        same (list[int]): List of cell-type IDs considered equivalent
        other (list[int]): List of cell-type IDs considered different
        glue (list[int]): Optional list of voxels which may connect equivalent types of cells, e.g. `dycpm.ECM`.

    Returns:
        float:  Neighbor index of the state
    """
    lattice = state.lattice()

    values = [] # sample of fractions
    for cell_id, cell in state.cells.items():
        if cell.type_id not in same: continue
        visited = [cell_id]
        count = 0
        total = 0

        voxels = cell.voxel_ids.copy()
        glued = [] # visited glue voxels
        while len(voxels) > 0:
            vid = voxels.pop()
            ref = state.voxel_coordinates(vid)
            for i in range(-1, 2):
                for j in range(-1, 2):
                    for k in range(-1, 2):
                        xyz = np.array([i, j, k], dtype=int) + ref
                        if np.any(xyz < 0) or np.any(xyz >= state.side): continue

                        cid = lattice[tuple(xyz)] # cell ID or medium type
                        if cid in glue:
                            tmp = state.voxel_id(xyz)
                            if tmp not in glued:
                                voxels.append(tmp)
                                glued.append(tmp)
                            continue
                        if cid in visited: continue
                        visited.append(cid)

                        type_id = state.value_type(cid)
                        if type_id in other:
                            count += 1
                        elif type_id not in same:
                            continue
                        total += 1
        if count == 0 and total == 0:
            values.append(0)
        else:
            values.append(count / total)
    return np.mean(values)

def bead_neighbors(state):
    """Return bead statistics (experimental)

    Examples:
        `bead_stats = bead_neighbors(state)`
    """
    lattice = state.lattice()
    neighbors = {}

    for ref in state.special_coordinates("bead"):
        for i in range(-1, 2):
            for j in range(-1, 2):
                for k in range(-1, 2):
                    xyz = ref + np.array([i, j, k], dtype=int)
                    if np.any(xyz < 0) or np.any(xyz >= state.side): continue

                    cid = lattice[tuple(xyz)]
                    if cid <= 0 or cid in neighbors.keys(): continue
                    neighbors[cid] = state.cells[cid].type_id
    npre = len(list(filter(lambda x: x == PRE, neighbors.values())))
    return [len(neighbors) - npre, npre]

class CellGraph(nx.Graph):
    """Graph of cell connectivity"""

    def __init__(self, type_id, state, glue = []):
        """Construct graph of a given cell type from a state"""
        super().__init__()
        self._type_id = type_id
        self._glue = glue
        self._build(state)

    def update(self, state):
        """Update the graph using a new state"""
        self.clear()
        self._build(state)

    def _build(self, state):
        """Build the graph using a given state"""
        lattice = state.lattice()

        visited_epi = []
        for cell_id, cell in state.cells.items():
            if cell.type_id == self._type_id:
                self.add_node(cell_id)
                visited_epi.append(cell_id)

                visited_cached = visited_epi.copy()
                voxels = cell.voxel_ids.copy()
                glued = []
                while len(voxels) > 0:
                    vid = voxels.pop()
                    ref = state.voxel_coordinates(vid)
                    for i in range(-1, 2):
                        for j in range(-1, 2):
                            for k in range(-1, 2):
                                xyz = np.array([i, j, k], dtype=int) + ref
                                if np.any(xyz < 0) or np.any(xyz >= state.side): continue

                                cid = lattice[tuple(xyz)]
                                if cid in self._glue:
                                    tmp = state.voxel_id(xyz)
                                    if tmp not in glued:
                                        voxels.append(tmp)
                                        glued.append(tmp)
                                    continue
                                if cid in visited_cached: continue
                                visited_cached.append(cid)

                                if state.value_type(cid) == self._type_id: self.add_edge(cell_id, cid)

    def average_degree(self):
        """Calculate average degree of nodes in the graph"""
        return np.mean([d for _, d in self.degree()])

//! Simulation data layout
extern crate serde;
extern crate serde_json;
use mt19937; // Adapted Mersenne twister implementation
use super::*;

/// Number of cell types
pub const CELL_TYPES_NUMBER: usize = 2;
/// Voxel type ID for medium
pub const MEDIUM: isize = 0;
/// Voxel type of the extracellular matrix
pub const ECM: isize = -1isize;
/// Voxel type ID for simulations with a "bead" (experimental)
pub const BEAD: isize = -3;
/// Voxel type ID of the epiblast cells
pub const EPI: isize = 1;
/// Voxel type ID of the preendoderm cells
pub const PRE: isize = 2;
/// Return default values for the ECM production and degradation exponents
pub fn default_ecm_parameters() -> [f64; 5] {
    [f64::MAX, f64::NEG_INFINITY, f64::NEG_INFINITY, f64::NEG_INFINITY, f64::NEG_INFINITY]
}

/// Description of a cell state
#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct Cell {
    /// Cell type: [`EPI`] or [`PRE`]
    pub type_id: isize,
    /// ID of the mother cell
    pub parent_id: usize,
    /// Current age
    pub age: usize,
    /// Preferred volume
    pub preferred_volume: f64,
    /// Division volume
    pub division_volume: f64,
    /// IDs of occupied voxels
    pub voxel_ids: std::collections::HashSet<i64>
}
impl Cell {
    /// Return actual volume of the cell
    pub fn get_volume(&self) -> usize { self.voxel_ids.len() }
}

/// Voxel identificator
pub type VoxelId = i64;
/// Type alias of lattice values
pub type VoxelValue = isize;

/// Simulation data
#[derive(Clone, serde::Serialize, serde::Deserialize)]
pub struct State {
    /// Size of the simulation lattice side
    pub side: usize,

    /// Counter of simulation steps
    pub time: usize,

    /// A map between IDs of the cells and their data
    ///
    /// A cell ID must be greater than zero.
    pub cells: std::collections::HashMap<usize, Cell>,

    /// Voxel ids of the ECM
    #[serde(default)]
    pub ecm: std::collections::HashSet<VoxelId>,

    /// Paremeters that characterize ECM
    ///
    /// The following order applise:
    ///
    /// 0. The energy penalty per volume of ECM;
    /// 1. Active exponent of transitions from `MEDIUM` to `ECM`;
    /// 2. Active exponent of transitions from `EPI` to `ECM`;
    /// 3. Active exponent of transitions from `PRE` to `ECM`;
    /// 4. Active exponent of transitions from `ECM` to `ECM`.
    #[serde(default = "default_ecm_parameters")]
    pub ecm_parameters: [f64; 5],

    /// Escape probability for three voxel types
    ///
    /// 0. Medium (cf. [`MEDIUM`])
    /// 1. Epi cells (cf. [`EPI`])
    /// 2. PrE cells (cf [`PRE`])
    pub action_probabilities: [f64; 3],
    /// Cell death probability
    pub death_probability: f64,

    /// Growth rate of two cell types
    ///
    /// 0. Epi cells
    /// 1. PrE cells
    pub growth_rate: [f64; CELL_TYPES_NUMBER],
    /// Empirical distribution of division volumes
    pub division_distribution: Vec<f64>,

    /// Volume elasticity of two cell types
    ///
    /// 0. EPI cells
    /// 1. PrE cells
    pub volume_elasticity: [f64; CELL_TYPES_NUMBER],

    /// Active exponents
    ///
    /// 0. Medium to EPI
    /// 1. Medium to PrE
    /// 2. EPI to Medium
    /// 3. EPI to EPI
    /// 4. EPI to PrE
    /// 5. PrE to Medium
    /// 6. PrE to EPI
    /// 7. PrE to PrE
    #[serde(default)]
    pub active_exponents: [f64; 8],

    /// Surface tension between 3 voxel types
    ///
    /// 0. Medium/EPI
    /// 1. Medium/PrE
    /// 2. EPI/EPI
    /// 3. EPI/PrE
    /// 4. PrE/PrE
    pub surface_tension: [f64; 5],
    /// Spin interaction modulation: face neighbors, edge neighbors, and node neighbors
    pub modulation: [f64; 3],

    /// IDs of bead voxels (experimental)
    pub bead: std::collections::HashSet<VoxelId>,
    /// Surface-tension coefficients for medium, EPI, and PrE.
    pub bead_parameters: [f64; 3],

    /// State of the random number generator
    #[serde(default)]
    pub rng: mt19937::MT19937
}
impl State {
    /// Seed random-number generator
    pub fn seed(&mut self, seed: u32) { self.rng.seed(seed); }

    /// Validate data and compile a lattice of voxel values
    pub(crate) fn compile(&self) -> Result<Vec<VoxelValue>, String> {
        // Simple range checks
        if self.side == 0usize {
            return Err(String::from("Lattice side must be greater than zero"))
        }
        for value in self.surface_tension {
            if value <= 0f64 {
                return Err(String::from("Surface tension must be greater than zero"))
            }
        }
        for value in self.volume_elasticity {
            if value <= 0f64 {
                return Err(String::from("Volume elasticity must be greater than zero"))
            }
        }
        for value in self.action_probabilities {
            if value <= 0f64 || value >= 1f64 {
                return Err(String::from("Escape probabilities must be in the interval `(0, 1)`"))
            }
        }
        for value in self.growth_rate {
            if value < 0f64 {
                return Err(String::from("Growth rate must be greater than or equal to zero"))
            }
        }
        if self.death_probability < 0f64 {
            return Err(String::from("Death rate must be greater than or equal to zero"))
        }
        for value in &self.division_distribution {
            if *value <= 2f64 {
                return Err(String::from("Sample of division volume must contain values greater than two"))
            }
        }
        for value in self.bead_parameters {
            if value <= 0f64 {
                return Err(String::from("Surface tension of bead must be greater than zero"))
            }
        }

        // By compling the lattice guard against doubly occupied voxels
        let mut lattice = vec![0 as VoxelValue; self.side.pow(3)];
        for key in self.cells.keys() { // Cycle over cells
            if *key == 0usize { return Err(String::from("Cell ID `0` is not allowed")) }
            let cell = &self.cells[key];

            if cell.type_id != EPI && cell.type_id != PRE {
                return Err(format!(
                    "Cell `{key}` has an unknown type {type_id}", type_id = cell.type_id
                ))
            }
            if cell.preferred_volume <= 0f64 {
                return Err(format!("Cell `{key}` has a non-positive preferred volume"))
            }

            // Check voxel occupation
            for voxel_id in &cell.voxel_ids {
                if *voxel_id < 0 || *voxel_id >= self.side.pow(3) as i64 {
                    return Err(format!("Voxel `{voxel_id}` of cell `{key}` is out of bounds"))
                }
                if lattice[*voxel_id as usize] != 0 {
                    return Err(format!(
                        "Double occupation of a voxel `{voxel}` by cells `{key}` and `{value}`",
                        voxel = *voxel_id, value = lattice[*voxel_id as usize]
                    ))
                }
                lattice[*voxel_id as usize] = *key as VoxelValue;
            }
        }

        for voxel_id in &self.ecm {
            if *voxel_id < 0 || *voxel_id >= self.side.pow(3) as i64 {
                return Err(format!("Voxel `{voxel_id}` of ECM is out of bounds"))
            }
            if lattice[*voxel_id as usize] != 0 {
                return Err(format!(
                    "Double occupation of a voxel `{voxel}` by ECM and cell `{value}`",
                    voxel = *voxel_id, value = lattice[*voxel_id as usize]
                ))
            }
            lattice[*voxel_id as usize] = ECM;
        }

        for voxel_id in &self.bead {
            if *voxel_id < 0 || *voxel_id >= self.side.pow(3) as i64 {
                return Err(format!("Voxel `{voxel_id}` of BEAD is out of bounds"))
            }
            if lattice[*voxel_id as usize] != 0 {
                panic!("Freezing a voxel (#{:?}) occupied by (#{:?})", voxel_id, lattice[*voxel_id as usize])
            }
            lattice[*voxel_id as usize] = BEAD;
        }

        Ok(lattice)
    }

    /// Read data from a JSON file
    pub fn from_json_file(file: &str) -> Self {
        serde_json::from_reader(
            std::io::BufReader::new(
                std::fs::File::open(std::path::Path::new(file)).unwrap()
            )
        ).unwrap()
    }

    /// Return active exponents of a transition from one voxel-value type to another
    pub(crate) fn get_active_exponent(&self, voxel_type1: isize, voxel_type2: isize, ecm: bool) -> f64 {
        if ecm {
            match (voxel_type1, voxel_type2) {
                (MEDIUM, ECM) => return self.ecm_parameters[1],
                (EPI, ECM) => return self.ecm_parameters[2],
                (PRE, ECM) => return self.ecm_parameters[3],
                (ECM, ECM) => return self.ecm_parameters[4],
                _ => ()
            }
        }
        self.active_exponents[
            match (voxel_type1, voxel_type2) {
                (MEDIUM | ECM, ECM | MEDIUM) => return 0f64,
                (MEDIUM | ECM, EPI) => 0,
                (MEDIUM | ECM, PRE) => 1,
                (EPI, MEDIUM | ECM) => 2,
                (EPI, EPI) => 3,
                (EPI, PRE) => 4,
                (PRE, MEDIUM | ECM) => 5,
                (PRE, EPI) => 6,
                (PRE, PRE) => 7,
                _ => panic!("Unknown type of active exponents {:?}", (voxel_type1, voxel_type2))
            }
        ]
    }

}

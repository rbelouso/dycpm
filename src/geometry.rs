/// Number of physical dimensions
pub const DIM: usize = 3;

/// Number pi over 2
const PI_2: f64 = std::f64::consts::PI / 2f64;
/// Number pi over 3
const PI_3: f64 = std::f64::consts::PI / 3f64;
/// Target machine accuracy
const EPSILON: f64 = 10f64 * std::f64::EPSILON;

/// Return maximum eigenvalue of the gyration tensor tensor
pub(crate) fn calc_principle_value(tog: &[f64; 2 * DIM]) -> f64 {
    let a2 = tog[0].powi(2);
    let b2 = tog[1].powi(2);
    let c2 = tog[2].powi(2);
    let d2 = tog[3].powi(2);
    let e2 = tog[4].powi(2);
    let f2 = tog[5].powi(2);

    let t1 = 2f64 * tog[0] - tog[1] - tog[2];
    let t2 = 2f64 * tog[1] - tog[0] - tog[2];
    let t3 = 2f64 * tog[2] - tog[0] - tog[1];

    let x1 = a2 + b2 + c2 + 3f64 * (d2 + e2 + f2)
        - tog[0] * tog[1] - tog[0] * tog[2] - tog[1] * tog[2];
    let x2 = - t1 * t2 * t3 + 9f64 * (t1 * e2 + t2 * f2 + t3 * d2)
        - 54f64 * tog[3] * tog[4] * tog[5];

    let mut phi = if x2.abs() < EPSILON {
        PI_2
    } else {
        ((4f64 * x1.powi(3) - x2.powi(2)).sqrt() / x2).atan()
    };
    if phi < 0f64 { phi += std::f64::consts::PI; }
    phi /= 3f64;

    let trace3 = (tog[0] + tog[1] + tog[2]) / 3f64;
    let c = if x1.abs() < EPSILON {
        0f64
    } else {
        2f64 * x1.sqrt() / 3f64
    };
    return (
        trace3 - c * phi.cos()
    ).max(
        trace3 + c * (phi - PI_3).cos()
    ).max(
        trace3 + c * (phi + PI_3).cos()
    )
}
/// Return vector dot product
pub(crate) fn vector_dot(v1: &[f64; DIM], v2: &[f64; DIM]) -> f64 {
    v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2]
}
/// Return vector cross product
pub(crate) fn vector_cross(v1: &[f64; DIM], v2: &[f64; DIM]) -> [f64; DIM] {
    [
        v1[1] * v2[2] - v1[2] * v2[1],
        v1[2] * v2[0] - v1[0] * v2[2],
        v1[0] * v2[1] - v1[1] * v2[0]
    ]
}
/// Return vector squared norm
pub(crate) fn vector_squared_norm(vector: &[f64; DIM]) -> f64 {
    vector[0].powi(2) + vector[1].powi(2) + vector[2].powi(2)
}

// /// Return vector difference
// pub(crate) fn vector_difference(vector1: &[f64; DIM], vector2: &[f64; DIM]) -> [f64; DIM] {
//     [vector1[0] - vector2[0], vector1[1] - vector2[1], vector1[2] - vector2[2]]
// }

/// Return eigenvector (not normalized) for the maximum eigenvalue of the gyration tensor
pub(crate) fn calc_principle_axis(tog: &[f64; 2 * DIM]) -> [f64; DIM] {
    let value = calc_principle_value(tog);

    let vec1 = [tog[0] - value, tog[3], tog[5]];
    let vec2 = [tog[3], tog[1] - value, tog[4]];

    let axis = vector_cross(&vec1, &vec2);
    if vector_squared_norm(&axis) < EPSILON {
        let sqr2 = vector_squared_norm(&vec2);
        if sqr2 < EPSILON { return [1f64, 0f64, 0f64] }
        let mu = vector_dot(&vec1, &vec2) / sqr2;
        return [1f64, -mu, 0f64]
    }

    axis
}

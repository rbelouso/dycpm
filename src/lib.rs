//! A proof of the concept for the dynamic cellular Potts model
extern crate rand;
extern crate rand_distr;
extern crate serde;
extern crate serde_json;
extern crate pyo3;
use pyo3::prelude::*;
extern crate pythonize;
use std::iter::FromIterator;

mod mt19937; // Adapted Mersenne twister implementation
mod geometry;
pub mod data;
pub mod simulation;

use geometry::*;
use data::*;
use simulation::*;

#[cfg(test)]
mod tests;

/// [https://stackoverflow.com/questions/28392008/more-concise-hashmap-initialization]
#[macro_export]
macro_rules! hashmap {
    ($( $key: expr => $val: expr ),*) => {{
        #[allow(unused_mut)]
        let mut map = ::std::collections::HashMap::new();
        $( map.insert($key, $val); )*
        map
    }}
}
#[macro_export]
macro_rules! hashset {
    ($( $val: expr ),*) => {{
        #[allow(unused_mut)]
        let set = ::std::collections::HashSet::new();
        $( set.insert($val); )*
        set
    }}
}

/// Python module `pycpm`
#[pymodule]
fn dycpm(_py: Python<'_>, m: &PyModule) -> PyResult<()> {
    m.add("MEDIUM", MEDIUM)?;
    m.add("ECM", ECM)?;
    m.add("EPI", EPI)?;
    m.add("PRE", PRE)?;
    m.add_class::<Kernel>()?;
    Ok(())
}

use super::*;

/// Direction vectors of the neighborhood from which the target values are chosen
pub(crate) const TARGET_NEIGHBORHOOD: [[i64; DIM]; 6] = [
    [0, 0, 1], [0, 0, -1], [0, 1, 0], [0, -1, 0], [1, 0, 0], [-1, 0, 0]
];
/// Direction vectors of the neighborhood involved in contact interactions
pub(crate) const COUPLING_NEIGHBORHOOD: [[i64; DIM]; 26] = [
    [-1, -1, -1], [-1, -1, 0], [-1, -1, 1],
    [-1, 0, -1], [-1, 0, 0], [-1, 0, 1],
    [-1, 1, -1], [-1, 1, 0], [-1, 1, 1],
    [0, -1, -1], [0, -1, 0], [0, -1, 1],
    [0, 0, -1], [0, 0, 1],
    [0, 1, -1], [0, 1, 0], [0, 1, 1],
    [1, -1, -1], [1, -1, 0], [1, -1, 1],
    [1, 0, -1], [1, 0, 0], [1, 0, 1],
    [1, 1, -1], [1, 1, 0], [1, 1, 1]
];

/// Directions of adjacency plane
pub(crate) type AdjacencyPlane = [[i64; DIM]; 4];

/// Adjacency plane across X
pub(crate) const ADJACENCY_X: AdjacencyPlane = [
    [0, 0, 1], [0, 0, -1], [0, 1, 0], [0, -1, 0]
];
/// Adjacency plane across Y
pub(crate) const ADJACENCY_Y: AdjacencyPlane = [
    [0, 0, 1], [0, 0, -1], [1, 0, 0], [-1, 0, 0]
];
/// Adjacency plane across Z
pub(crate) const ADJACENCY_Z: AdjacencyPlane = [
    [0, 1, 0], [0, -1, 0], [1, 0, 0], [-1, 0, 0]
];

/// A stable implementation of the softmax function with multiplication in place
pub(crate) fn softmax_(slice: &mut [f64]) {
    let max = slice.iter().fold(0f64, |max, x| if *x > max { *x } else { max });
    for n in 0..slice.len() {
        slice[n] -= max;
        slice[n] = slice[n].exp();
    }
    
    let sum: f64 = slice.iter().sum();
    // if sum.is_infinite() {
    //     for n in 0..slice.len() {
    //         if slice[n].is_finite() { slice[n] = 0f64 };
    //     }
    //     return;
    // } // else

    for n in 0..slice.len() { slice[n] /= sum; }
}

/// ID of voxels outside of the simulation box
pub(crate) const BOUNDARY_VOXEL: VoxelId = -1;
/// Voxel descriptor
pub(crate) struct Voxel {
    /// Voxel ID
    id: VoxelId,
    /// 3D index of voxel
    index: [i64; DIM]
}
impl PartialEq for Voxel {
    fn eq(&self, other: &Self) -> bool {
        self.index == other.index
    }
}

/// Description of a voxel's value change
pub(crate) struct Change {
    /// Affected voxel
    pub(crate) voxel: Voxel,

    /// Target values of the voxel's sate
    pub(crate) target_values: Vec<VoxelValue>,
    /// Target probability weights
    pub(crate) possibilities: Vec<f64>
}

/// Simulation kernel
#[pyclass]
pub struct Kernel {
    /// Simulation state
    pub(crate) state: State,
    /// Square of the lattice side ([`Kernel::state::side`])
    pub(crate) side2: usize,
    /// Array of voxel values
    pub(crate) lattice: Vec<VoxelValue>,
    /// ID value assigned to the next daughter cell
    pub(crate) next_id: usize,
    /// Cached probabilities of division volumes
    pub(crate) division_probabilities: Vec<f64>,

    /// Array of active voxel id
    pub(crate) front: std::collections::HashSet<VoxelId>,

    /// Uniform random number generator
    random: rand::distributions::Uniform<f64>,

    /// Proposed changes
    pub(crate) changes: Vec<Change>
}
impl Kernel {
    /// Construct a simulation kernel for given state
    pub fn from_state(state: State) -> Kernel {
        let side2 = state.side.pow(2);
        let lattice = state.compile().unwrap();
        let next_id = state.cells.keys().max().unwrap() + 1usize;
        let division_probabilities = vec![0f64; state.division_distribution.len()];

        let mut kernel = Kernel {
            state, side2, lattice, next_id, division_probabilities, front: hashset!{},
            random: rand::distributions::Uniform::new(0f64, 1f64), changes: Vec::new()
        };

        kernel.state.division_distribution.sort_unstable_by(|a, b| a.partial_cmp(b).unwrap());
        for i in 0..kernel.division_probabilities.len() {
            kernel.division_probabilities[i] =
                (i + 1) as f64 / kernel.division_probabilities.len() as f64
        }

        // Find active voxels
        for voxel_id in 0..kernel.lattice.len() as i64 {
            if kernel.is_active(&kernel.get_voxel_by_id(voxel_id)) {
                kernel.front.insert(voxel_id);
            }
        }

        kernel
    }

    // /// Return size of the simulation-box side
    // pub fn side(&self) -> usize { self.state.side }
    // /// Return simulation time
    // pub fn time(&self) -> usize { self.state.time }
    // /// Return state as a JSON string
    // pub fn json_state(&self) -> String { self.state.to_json_string() }

    /// Convert a voxel ID into its 3D index
    pub(crate) fn id_to_index(&self, voxel_id: i64) -> [i64; DIM] {
        if voxel_id < 0 || voxel_id >= self.lattice.len() as i64 {
            panic!("Invalid voxel ID ({})", voxel_id)
        }
        let rem = voxel_id % self.side2 as i64;
        [rem % self.state.side as i64, rem / self.state.side as i64, voxel_id / self.side2 as i64]
    }
    /// Convert a voxel 3D index to its ID
    pub(crate) fn index_to_id(&self, i: i64, j: i64, k: i64) -> i64 {
        let side = self.state.side as i64;
        if 0 <= i && i < side && 0 <= j && j < side && 0 <= k && k < side {
            return i + j * self.state.side as i64 + k * self.side2 as i64;
        }
        BOUNDARY_VOXEL
    }
    /// Construct a voxel descriptor by its ID
    pub(crate) fn get_voxel_by_id(&self, id: i64) -> Voxel {
        Voxel { id, index: self.id_to_index(id) }
    }

    /// Set voxel's value
    pub(crate) fn set_voxel_value(&mut self, voxel: &Voxel, value: VoxelValue) {
        let old_value = self.lattice[voxel.id as usize];
        if old_value > 0 {
            self.state.cells.get_mut(&(old_value as usize)).unwrap().voxel_ids.remove(&voxel.id);
        } else if old_value == ECM {
            self.state.ecm.remove(&voxel.id);
        }
        if value > 0 {
            self.state.cells.get_mut(&(value as usize)).unwrap().voxel_ids.insert(voxel.id);
        } else if value == ECM {
            self.state.ecm.insert(voxel.id);
        }
        
        self.lattice[voxel.id as usize] = value;
        self.activate_neighborhood(voxel);
    }

    /// Return value of a voxel identified by its 3D index
    ///
    /// All voxels out of bounds are considered [`MEDIUM`].
    pub(crate) fn get_index_value(&self, i: i64, j: i64, k: i64) -> VoxelValue {
        let voxel_id = self.index_to_id(i, j, k);
        if 0 <= voxel_id && voxel_id < self.lattice.len() as i64 {
            return self.lattice[voxel_id as usize]
        }
        MEDIUM
    }

    /// Return type of the voxel value: [`MEDIUM`], [`ECM`], [`EPI`], or [`PRE`]
    pub(crate) fn get_value_type(&self, voxel_value: VoxelValue) -> isize {
        if voxel_value > 0 { return self.state.cells[&(voxel_value as usize)].type_id }
        voxel_value
    }

    /// Utility function to substitute ECM type with PRE
    fn ecm_to_pre(type_id: isize) -> isize {
        if type_id == ECM { return PRE }
        type_id
    }

    /// Return surface tension between two voxel types
    pub(crate) fn get_surface_tension(&self, voxel_value1: VoxelValue, voxel_value2: VoxelValue) -> f64 {
        if voxel_value1 == voxel_value2 { return 0f64 }
        let voxel_type1 = Self::ecm_to_pre(self.get_value_type(voxel_value1));
        let voxel_type2 = Self::ecm_to_pre(self.get_value_type(voxel_value2));

        let min = voxel_type1.min(voxel_type2);
        let max = voxel_type1.max(voxel_type2);
        match min {
            BEAD => {
                return self.state.bead_parameters[max as usize]
            }
            MEDIUM => {
                // Not happening: if max == 0usize { return 0f64 } // zero tension in medium
                return self.state.surface_tension[max as usize - 1]
            }
            _ => self.state.surface_tension[(min + max) as usize]
        }
    }

    /// Return an active exponent for one voxel value to another
    pub(crate) fn get_active_exponent(&self, voxel_value1: VoxelValue, voxel_value2: VoxelValue, ecm: bool) -> f64 {
        self.state.get_active_exponent(self.get_value_type(voxel_value1), self.get_value_type(voxel_value2), ecm)
    }

    /// Calculate energy penalty for an incremented volume of the given voxel value
    ///
    /// The increment can be negative, zero, or positive.
    pub(crate) fn calc_volume_penalty(&self, voxel_value: VoxelValue, increment: f64) -> f64 {
        if voxel_value == MEDIUM || voxel_value == BEAD { return 0f64 }
        if voxel_value == ECM { return increment * self.state.ecm_parameters[0] }
        let cell = &self.state.cells[&(voxel_value as usize)];
        self.state.volume_elasticity[cell.type_id as usize - 1] * (
            cell.get_volume() as f64 + increment - cell.preferred_volume
        ).powi(2) / 2f64
    }

    /// Count voxels in adjacency plane
    fn plane_index(&self, voxel: &Voxel, voxel_value: VoxelValue, adjacency: &AdjacencyPlane) -> usize {
        let mut result = 0;

        for triplet in adjacency {
            if voxel_value == self.get_index_value(
                voxel.index[0] + triplet[0],
                voxel.index[1] + triplet[1],
                voxel.index[2] + triplet[2]
            ) { result += 1 }
        }

        result
    }
    /// Calculate planar index
    pub(crate) fn planar_index(&self, voxel: &Voxel, voxel_value: VoxelValue) -> [usize; DIM] {
        let mut result = [0; DIM];

        for (index, adjacency) in [&ADJACENCY_X, &ADJACENCY_Y, &ADJACENCY_Z].iter().enumerate() {
            result[index] = self.plane_index(voxel, voxel_value, adjacency)
        }
        result.sort();

        result
    }

    // /// Check whether local connectivity of a voxel in an adjacency plane
    // pub(crate) fn check_plane(&self, voxel: &Voxel, voxel_value: usize, adjacency: &AdjacencyPlane) -> bool {
    //     // The implementation relies on a graph connectivity check
    //     let mut graph_nodes = Vec::<[i64; DIM]>::with_capacity(4);
    //     for triplet in adjacency {
    //         if voxel_value == self.get_index_value(
    //             voxel.index[0] + triplet[0],
    //             voxel.index[1] + triplet[1],
    //             voxel.index[2] + triplet[2]
    //         ) { graph_nodes.push(*triplet) }
    //     }
    //     // Trivial situtations
    //     let sz = graph_nodes.len();
    //     match sz {
    //         1 => return true,
    //         2 | 3 | 4 => (),
    //         _ => panic!("Unexpected plane index {}", sz)
    //     }

    //     // Find edges
    //     let mut graph_edges = vec![Vec::<usize>::new(); sz];
    //     for m in 0..sz - 1 {
    //         for n in m + 1..sz {
    //             let edge = [
    //                 graph_nodes[m][0] + graph_nodes[n][0],
    //                 graph_nodes[m][1] + graph_nodes[n][1],
    //                 graph_nodes[m][2] + graph_nodes[n][2]
    //             ];
    //             if edge[0] == 0 && edge[1] == 0 && edge[2] == 0 { continue }

    //             // else
    //             if voxel_value == self.get_index_value(
    //                 voxel.index[0] + edge[0],
    //                 voxel.index[1] + edge[1],
    //                 voxel.index[2] + edge[2]
    //             ) {
    //                 graph_edges[m].push(n);
    //                 graph_edges[n].push(m);
    //             }

    //         }
    //     }

    //     // Apply the breadth-first search over the graph to check its full connectivity
    //     let mut visited = vec![false; sz];
    //     let mut stack = Vec::<usize>::with_capacity(sz - 1);
    //     stack.push(0usize);
    //     while stack.len() > 0 {
    //         let node = stack.pop().unwrap();
    //         visited[node] = true;
    //         for edge in &graph_edges[node] { if !visited[*edge] { stack.push(*edge) } }
    //     }

    //     visited.iter().all(|v| *v)
    // }

    /// Check whether local connectivity of a voxel allows a move
    pub(crate) fn check_change(&self, voxel: &Voxel, voxel_value: VoxelValue) -> bool {
        // First screen planar index
        let index = self.planar_index(voxel, voxel_value);
        match index {
            [0, 0, 0] | [0, 2, 2] | [2, 2, 4] | [4, 4, 4] => return false,
            [0, 1, 1] => return true,
            [1, 1, 2] | [1, 2, 3] | [2, 2, 2] | [2, 3, 3] | [3, 3, 4] => (),
            _ => panic!("Unexpected planar index ({}, {}, {})", index[0], index[1], index[2])
        }

        let mut graph_nodes = Vec::<[i64; DIM]>::with_capacity(5);
        for triplet in &TARGET_NEIGHBORHOOD {
            if voxel_value == self.get_index_value(
                voxel.index[0] + triplet[0],
                voxel.index[1] + triplet[1],
                voxel.index[2] + triplet[2]
            ) { graph_nodes.push(*triplet) }
        }
        // Trivial situtations
        let sz = graph_nodes.len();

        // Find edges
        let mut graph_edges = vec![Vec::<usize>::new(); sz];
        for m in 0..sz - 1 {
            for n in m + 1..sz {
                let edge = [
                    graph_nodes[m][0] + graph_nodes[n][0],
                    graph_nodes[m][1] + graph_nodes[n][1],
                    graph_nodes[m][2] + graph_nodes[n][2]
                ];
                if edge[0] == 0 && edge[1] == 0 && edge[2] == 0 { continue }

                // else
                if voxel_value == self.get_index_value(
                    voxel.index[0] + edge[0],
                    voxel.index[1] + edge[1],
                    voxel.index[2] + edge[2]
                ) {
                    graph_edges[m].push(n);
                    graph_edges[n].push(m);
                }

            }
        }

        // Apply the breadth-first search over the graph to check its full connectivity
        let mut visited = vec![false; sz];
        let mut stack = Vec::<usize>::with_capacity(sz - 1);
        stack.push(0usize);
        while stack.len() > 0 {
            let node = stack.pop().unwrap();
            visited[node] = true;
            for edge in &graph_edges[node] { if !visited[*edge] { stack.push(*edge) } }
        }

        visited.iter().all(|v| *v)
    }

    /// Modulation of voxel-voxel interactions by distance
    pub(crate) fn modulate(&self, triplet: &[i64; DIM]) -> f64 {
        match triplet[0].abs() + triplet[1].abs() + triplet[2].abs() {
            1 => self.state.modulation[0],
            2 => self.state.modulation[1],
            3 => self.state.modulation[2],
            _ => 0.
        }
    }

    /// Propose a change of the voxel's state
    ///
    /// The parameter `voxel_id` must refer to a voxel within the simulated lattice.
    pub(crate) fn propose_change(&mut self, voxel_id: i64) {
        let voxel = self.get_voxel_by_id(voxel_id);
        let voxel_value = self.lattice[voxel_id as usize];
        let voxel_type = self.get_value_type(voxel_value);

        // Determine target values
        let mut target_values = Vec::<VoxelValue>::with_capacity(5);
        target_values.push(voxel_value);
        for triplet in &TARGET_NEIGHBORHOOD {
            let neighbor_index = [
                voxel.index[0] + triplet[0],
                voxel.index[1] + triplet[1],
                voxel.index[2] + triplet[2]
            ];
            let neighbor_id = self.index_to_id(neighbor_index[0], neighbor_index[1], neighbor_index[2]);
            if self.state.bead.contains(&neighbor_id) { continue }

            let neighbor_value = self.get_index_value(neighbor_index[0], neighbor_index[1], neighbor_index[2]);
            if neighbor_value != voxel_value && !target_values.contains(&neighbor_value) {
                target_values.push(neighbor_value);
            }
        }
        if target_values.len() == 1 { // No target values except the current one
            self.front.remove(&voxel_id);
            return
        }
        // Production of ECM by PRE cell (when `target_values.len() > 1`, because only at the boundaries)
        let to_ecm = target_values.iter().any(|&x| self.get_value_type(x) == PRE);
        if to_ecm && !target_values.contains(&ECM) { target_values.push(ECM); }

        // Draw from escape probability
        let rank = rand_distr::Distribution::sample(&self.random, &mut self.state.rng);
        let action_probability = self.state.action_probabilities[Self::ecm_to_pre(voxel_type) as usize];
        if rank > action_probability { return } // no move

        // Calculate energy differences, starting with the contact interactions
        let mut energy_base = self.calc_volume_penalty(voxel_value, -1f64)
            - self.calc_volume_penalty(voxel_value, 0f64);
        for triplet in &COUPLING_NEIGHBORHOOD {
            energy_base -= self.modulate(&triplet) * self.get_surface_tension(
                voxel_value, self.get_index_value(
                    voxel.index[0] + triplet[0],
                    voxel.index[1] + triplet[1],
                    voxel.index[2] + triplet[2]
                )
            );
        }

        // First accumulate energy differences in a buffer
        let mut buffer = vec![0f64; target_values.len()];
        // buffer[0] = 1f64; // no calculations needed for the current voxel value
        for n in 1..buffer.len() {
            let target_value = target_values[n];
            buffer[n] = energy_base + self.calc_volume_penalty(target_value, 1f64)
                - self.calc_volume_penalty(target_value, 0f64);
            for triplet in &COUPLING_NEIGHBORHOOD {
                buffer[n] += self.modulate(&triplet) * self.get_surface_tension(
                    target_value, self.get_index_value(
                        voxel.index[0] + triplet[0],
                        voxel.index[1] + triplet[1],
                        voxel.index[2] + triplet[2]
                    )
                );
            }
        }

        // Second: accumulate weighting exponents
        for n in 1..buffer.len() {
            buffer[n] = (
                self.state.action_probabilities[
                    Self::ecm_to_pre(self.get_value_type(target_values[n])) as usize
                ] / action_probability
            ).ln() - buffer[n] + self.get_active_exponent(target_values[0], target_values[n], to_ecm);
        }

        self.changes.push(Change { voxel, /*rank,*/ target_values, possibilities: buffer });
    }

    /// Attempt to apply a change
    pub(crate) fn attempt_change(&mut self, change: &mut Change) {
        let voxel_value = self.lattice[change.voxel.id as usize];
        // A change might be unacceptable
        if voxel_value > 0 && !self.check_change(&change.voxel, voxel_value) { return }
        let l = change.target_values.len();
        for n in (1..l).rev() {
            // NB! Attempt to allow failure of connectivity for medium and ecm
            if change.target_values[n] > 0 && !self.check_change(&change.voxel, change.target_values[n]) {
                change.target_values.remove(n);
                change.possibilities.remove(n);
            }
        }
        if change.target_values.len() == 1 { return } // All values but the current one are blocked
        softmax_(&mut change.possibilities[..]);
        for p in &change.possibilities {
            if !p.is_finite() {
                panic!("Illegal value of probability ({})", p);
            }
        }

        // In general...
        let threshold = rand_distr::Distribution::sample(&self.random, &mut self.state.rng);
        let mut probability = 0f64; // Accumulated probability
        for n in 0..change.possibilities.len() {
            probability += change.possibilities[n];
            if threshold < probability { // Set the chosen value
                self.set_voxel_value(&change.voxel, change.target_values[n]);
                return
            }
        }
    }

    /// Return a cell's center of mass
    pub(crate) fn get_cell_com(&self, cell: &Cell) -> [f64; DIM] {
        let mut value = [0f64; DIM];
        let mut counter = 0usize; // Counter for an accurate floating-point averaging

        for voxel_id in &cell.voxel_ids {
            counter += 1;
            let index = self.id_to_index(*voxel_id);
            for i in 0..DIM { value[i] += (index[i] as f64 - value[i]) / counter as f64 }
        }

        value
    }

    /// Return a cell's gyration radius
    pub(crate) fn get_cell_gyradius(&self, cell: &Cell) -> f64 {
        // Calculate tog and COM reference frame
        let com = self.get_cell_com(&cell);
        let mut diag = [0f64; DIM];
        let mut counter = 0usize; // Counter for an accurate floating-point averaging
        for voxel_id in &cell.voxel_ids {
            counter += 1;
            let index = self.id_to_index(*voxel_id);
            let rcom = [index[0] as f64 - com[0], index[1] as f64 - com[1], index[2] as f64 - com[2]];
            for i in 0..DIM {
                diag[i] += (rcom[i].powi(2) - diag[i]) / counter as f64;
            }
        }

        return diag.iter().sum::<f64>().sqrt()
    }

    /// Return cell's interface index
    pub(crate) fn get_cell_ii(&self, cell: &Cell) -> f64 {
        let mut area = 0usize;
        for voxel_id in &cell.voxel_ids {
            let voxel = self.get_voxel_by_id(*voxel_id);
            let value = self.get_index_value(voxel.index[0], voxel.index[1], voxel.index[2]);
            for triplet in &TARGET_NEIGHBORHOOD {
                let neighbor_value = self.get_index_value(
                    voxel.index[0] + triplet[0],
                    voxel.index[1] + triplet[1],
                    voxel.index[2] + triplet[2]
                );
                if value != neighbor_value { area += 1usize }
            }
        }

        return area as f64 / cell.voxel_ids.len() as f64
    }

    /// Return a sample from the division volume distribution
    fn sample_division(&mut self) -> f64 {
        let sz = self.division_probabilities.len();
        let random = rand_distr::Distribution::sample(&self.random, &mut self.state.rng);
        let mut last = std::f64::NAN;
        let mut base = std::f64::NAN;

        for i in 0..sz {
            let now = self.division_probabilities[i];
            if random < now {
                return if i == 0 { self.state.division_distribution[0] } else {
                    base + (random - last) * (self.state.division_distribution[i] - base)
                    / (now - last)
                }
            } // else
            last = now; base = self.state.division_distribution[i];
        }

        *self.state.division_distribution.last().unwrap()
    }
    /// Apply cell death and division routines
    pub(crate) fn kill_and_divide(&mut self) {
        // Sample number of death events from Wi
        // Poisson generator based upon the inversion by sequential search: Devroye (1986)
        let mut deaths = 0usize;
        let mut p = self.state.death_probability.exp().recip();
        let mut s = p;
        let u = rand_distr::Distribution::sample(&self.random, &mut self.state.rng);
        while u > s {
            deaths += 1;
            p *= self.state.death_probability / deaths as f64;
            s += p;
        }

        let mut cell_ids: Vec<usize> = self.state.cells.keys().map(|key| *key).collect();
        if deaths > 0 { rand::seq::SliceRandom::shuffle(&mut cell_ids[..], &mut self.state.rng) }

        // let random = rand_distr::Distribution::sample(&self.random, &mut self.state.rng);
        // if random < self.state.death_probability {
        //     self.kill_cell(cell_id);
        //     continue
        // } // else

        for cell_id in cell_ids {
            if deaths > 0 {
                self.kill_cell(cell_id);
                deaths -= 1;
                continue
            }
            let cell = &self.state.cells[&cell_id];
            if (cell.get_volume() as f64) >= cell.division_volume {
                self.divide_cell(cell_id);
            }
        }
    }
    /// Kill a cell identified by its ID
    fn kill_cell(&mut self, cell_id: usize) {
        let cell = self.state.cells.remove(&cell_id).unwrap();

        for voxel_id in &cell.voxel_ids {
            self.lattice[*voxel_id as usize] = MEDIUM;
        }
    }
    /// Divide a cell identified by its ID
    ///
    /// The cell is assumed to have a sufficient size for division.
    fn divide_cell(&mut self, cell_id: usize) {
        let cell = self.state.cells.remove(&cell_id).unwrap();

        // Calculate tog and COM reference frame
        let com = self.get_cell_com(&cell);
        let mut tog = [0f64; 2 * DIM];
        let mut counter = 0usize; // Counter for an accurate floating-point averaging
        let mut rcoms = std::collections::HashMap::<i64, [f64; DIM]>::with_capacity(
            cell.voxel_ids.len()
        );
        for voxel_id in &cell.voxel_ids {
            counter += 1;
            let index = self.id_to_index(*voxel_id);
            rcoms.insert(*voxel_id, [
                index[0] as f64 - com[0], index[1] as f64 - com[1], index[2] as f64 - com[2]
            ]);

            let rcom = rcoms[voxel_id];
            for i in 0..DIM {
                tog[i] += (rcom[i].powi(2) - tog[i]) / counter as f64;
                tog[DIM + i] += (rcom[i] * rcom[(i + 1) % DIM] - tog[DIM + i]) / counter as f64;
            }
        }
        let axis = calc_principle_axis(&tog); // division axis

        // Prepare cells
        let mut cell_ids = vec![self.next_id, self.next_id + 1];
        self.next_id += 2;
        let mut cells = vec![
            Cell {
                type_id: cell.type_id, parent_id: cell_id, age: 0usize,
                preferred_volume: cell.preferred_volume / 2.0,
                division_volume: self.sample_division(),
                voxel_ids: hashset!{}
            },
            Cell {
                type_id: cell.type_id, parent_id: cell_id, age: 0usize,
                preferred_volume: cell.preferred_volume / 2.0,
                division_volume: self.sample_division(),
                voxel_ids: hashset!{}
            }
        ];

        // Find cell poles
        const NORTH: usize = 0;
        const SOUTH: usize = 1;
        let mut dots = std::collections::HashMap::<VoxelId, f64>::new();
        let mut poles = [
            *cell.voxel_ids.iter().filter(|voxel_id| vector_dot(&axis, &rcoms[voxel_id]) >= 0.0).next().unwrap(),
            *cell.voxel_ids.iter().filter(|voxel_id| vector_dot(&axis, &rcoms[voxel_id]) < 0.0).next().unwrap(),
        ];
        let mut paras = [0f64; 2];
        let mut perps = [f64::INFINITY; 2];
        for voxel_id in &cell.voxel_ids {
            let para = vector_dot(&axis, &rcoms[voxel_id]);
            let perp = vector_squared_norm(&vector_cross(&axis, &rcoms[voxel_id]));
            dots.insert(*voxel_id, para);
            if para >= paras[NORTH] {
                // On a discrete lattice an equality is actually possible
                if para == paras[NORTH] && perp >= perps[NORTH] { continue }
                paras[NORTH] = para;
                perps[NORTH] = perp;
                poles[NORTH] = *voxel_id;
            } else if para <= paras[SOUTH] {
                // On a discrete lattice an equality is actually possible
                if para == paras[SOUTH] && perp >= perps[SOUTH] { continue }
                paras[SOUTH] = para;
                perps[SOUTH] = perp;
                poles[SOUTH] = *voxel_id;
            }
        }
        if poles[NORTH] == poles[SOUTH] { panic!("Poles of a parent cell {} are not identified", cell_id) }
        
        let filter = |i: usize, voxel_id: VoxelId| match i {
            NORTH => dots[&voxel_id] >= 0.0,
            _ => dots[&voxel_id] < 0.0,
        };
        for i in 0..2 {
            let mut seeds = std::collections::VecDeque::<Voxel>::from_iter([self.get_voxel_by_id(poles[i])]);
            while !seeds.is_empty() {
                let seed = seeds.pop_front().unwrap();
                self.lattice[seed.id as usize] = cell_ids[i] as VoxelValue;
                cells[i].voxel_ids.insert(seed.id);

                for triplet in &TARGET_NEIGHBORHOOD {
                    let index = [
                        seed.index[0] + triplet[0],
                        seed.index[1] + triplet[1],
                        seed.index[2] + triplet[2]
                    ];
                    if cell_id as VoxelValue == self.get_index_value(index[0], index[1], index[2]) {
                        let voxel = self.get_voxel_by_id(self.index_to_id(index[0], index[1], index[2]));
                        if filter(i, voxel.id) && !seeds.contains(&voxel) { seeds.push_back(voxel); }
                    }
                }
            }
        }

        // Division debris and activation
        for voxel_id in &cell.voxel_ids {
            if !cells[0].voxel_ids.contains(voxel_id) && !cells[1].voxel_ids.contains(voxel_id) {
                self.lattice[*voxel_id as usize] = MEDIUM;
            }
            self.front.insert(*voxel_id);            
        }

        if cells[0].voxel_ids.len() == 0 || cells[1].voxel_ids.len() == 0 { panic!("Empty cell division") }

        // Finalize division by checking the connectivity
        while !cell_ids.is_empty() {
            if !self.check_connectivity(cells.last().unwrap()) {
                panic!("Connectivity broken by cell division")
            }
            self.state.cells.insert(cell_ids.pop().unwrap(), cells.pop().unwrap());
        }
    }

    /// Export state into a JSON file
    pub fn state_to_json(&self, file: &str) -> serde_json::Result<()> {
        serde_json::to_writer(
            std::io::BufWriter::new(
                std::fs::File::create(std::path::Path::new(file)).unwrap()
            ),
            &self.state
        )
    }
    /// Export state as [`State`] object
    pub fn to_state(&self) -> State { self.state.clone() }

    /// Check if a voxel is active
    fn is_active(&self, voxel: &Voxel) -> bool {
        if self.state.bead.contains(&voxel.id) { return false }
        let value = self.lattice[voxel.id as usize];
        for triplet in &TARGET_NEIGHBORHOOD {
            let neighbor_value = self.get_index_value(
                voxel.index[0] + triplet[0],
                voxel.index[1] + triplet[1],
                voxel.index[2] + triplet[2]
            );
            if value != neighbor_value { return true }
        }

        false
    }

    /// Check if a voxel is active
    fn activate_neighborhood(&mut self, voxel: &Voxel) {
        for triplet in &TARGET_NEIGHBORHOOD {
            let id = self.index_to_id(
                voxel.index[0] + triplet[0],
                voxel.index[1] + triplet[1],
                voxel.index[2] + triplet[2]
            );
            if !self.state.bead.contains(&id) && 0 <= id && id < self.lattice.len() as i64 { self.front.insert(id); }
        }
    }

    /// Check connectivity of a cell
    pub fn check_connectivity(&self, cell: &Cell) -> bool {
        // Apply the breadth-first search over the graph to check the full connectivity
        let voxel_ids = &cell.voxel_ids;
        let mut visited: std::collections::HashMap<VoxelId, bool> = hashmap!{};
        for id in voxel_ids { visited.insert(*id, false); }

        let mut stack = Vec::<VoxelId>::with_capacity(voxel_ids.len());
        stack.push(*voxel_ids.iter().next().unwrap());
        while stack.len() > 0 {
            let node = self.get_voxel_by_id(stack.pop().unwrap());
            visited.insert(node.id, true);
            for triplet in &TARGET_NEIGHBORHOOD {
                let id = self.index_to_id(
                    node.index[0] + triplet[0],
                    node.index[1] + triplet[1],
                    node.index[2] + triplet[2]
                );
                if voxel_ids.contains(&id) && !visited[&id] { stack.push(id) }
            }
        }

        visited.iter().all(|v| *v.1)
    }
}

#[pymethods]
impl Kernel {
    /// Python constructor to create a kernel with a given initial state
    #[new]
    pub fn new(state: PyObject, py: Python<'_>) -> Self {
        let dict = state.call_method0(py, "to_json").unwrap();
        Self::from_state(
            pythonize::depythonize(dict.extract(py).unwrap()).unwrap()
        )
    }

    /// Advance simulation by one step
    pub fn advance(&mut self) {
        self.kill_and_divide(); // Ensure that division conditions have been applied

        let front: Vec<VoxelId> = self.front.iter().copied().collect();
        for voxel_id in front {
            self.propose_change(voxel_id)
        }

        // Optionally sort changes by decreasing rank
        // self.changes.sort_by(|a, b| b.rank.partial_cmp(&a.rank).unwrap());
        while self.changes.len() > 0 {
            let mut change = self.changes.pop().unwrap();
            self.attempt_change(&mut change);
        }

        for cell in self.state.cells.values_mut() {
            cell.preferred_volume += self.state.growth_rate[cell.type_id as usize - 1];
            cell.age += 1usize;
        }
        self.state.time += 1;
    }

    /// Snapshot of the current state
    #[getter(state)]
    pub fn get_state<'py>(&self, py: Python<'py>) -> &'py PyAny {
        let state = pythonize::pythonize(py, &self.state).unwrap();
        let state_class = py.import("dycpm").unwrap().getattr("State").unwrap();
        state_class.call((), Some(state.extract(py).unwrap())).unwrap()
    }

    /// Current number of cells
    #[getter]
    pub fn cell_number(&self) -> usize { self.state.cells.len() }

    /// Cell gyradius
    pub fn cell_gyradius(&self, cell_id: usize) -> f64 {
        let cell = &self.state.cells[&cell_id];
        self.get_cell_gyradius(&cell)
    }

    /// Cell interface index
    pub fn cell_ii(&self, cell_id: usize) -> f64 {
        let cell = &self.state.cells[&cell_id];
        self.get_cell_ii(&cell)
    }

    /// Return volume of a given cell
    pub fn cell_volume(&self, cell_id: usize) -> usize {
        self.state.cells[&cell_id].get_volume()
    }

    /// Set preferred volume of a cell
    pub fn set_preferred_volume(&mut self, cell_id: usize, value: f64) {
        self.state.cells.get_mut(&cell_id).unwrap().preferred_volume = value;
    }

    /// Seed random number generator
    pub fn seed(&mut self, seed: u32) { self.state.seed(seed); }

    /// Check that cells are fragmentation-free
    pub fn check_connectivities(&self) -> bool {
        self.state.cells.values().all(|cell| self.check_connectivity(&cell))
    }

    /// Contact area between two cells
    pub fn contact_between(&self, c1: usize, c2: usize) -> usize {
        assert!(c1 > 0);
        let mut result = 0;

        for voxel_id in &self.state.cells[&c1].voxel_ids {
            for triplet in &TARGET_NEIGHBORHOOD {
                let voxel = self.get_voxel_by_id(*voxel_id);
                let neighbor = self.get_index_value(
                    voxel.index[0] + triplet[0],
                    voxel.index[1] + triplet[1],
                    voxel.index[2] + triplet[2]
                );
                if neighbor == c2 as isize { result += 1 }
            }
        }

        result
    }

    pub fn cell_area(&self, cell_id: usize) -> usize {
        assert!(cell_id > 0);
        let mut result = 0;

        for voxel_id in &self.state.cells[&cell_id].voxel_ids {
            for triplet in &TARGET_NEIGHBORHOOD {
                let voxel = self.get_voxel_by_id(*voxel_id);
                let neighbor = self.get_index_value(
                    voxel.index[0] + triplet[0],
                    voxel.index[1] + triplet[1],
                    voxel.index[2] + triplet[2]
                );
                if neighbor != cell_id as isize { result += 1 }
            }
        }

        result
    }
}

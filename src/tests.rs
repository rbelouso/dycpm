//! Crate unit tests
use super::*;
use std::iter::FromIterator;

/// Test coupling neighborhood
#[test]
fn coupling_neighborhood() {
    let len = COUPLING_NEIGHBORHOOD.len();
    for i in 0..len - 1 {
        for j in i + 1..len {
            assert!(COUPLING_NEIGHBORHOOD[i] != COUPLING_NEIGHBORHOOD[j]);
        }
    }
}

/// Test active exponents
#[test]
fn active_exponents() {
    let state = State {
        side: 3, time: 0, cells: hashmap![
            1usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([13i64])
            },
            2usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([14i64, 23i64])
            },
            11usize => Cell {
                type_id: PRE, preferred_volume: 2f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([25i64, 26i64])
            },
            12usize => Cell {
                type_id: PRE, preferred_volume: 1f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([1i64])
            }
        ], ecm: hashset![], ecm_parameters: default_ecm_parameters(),
        action_probabilities: [0.001, 0.999, 0.999], death_probability: 0f64,
        growth_rate: [0.5, 0.5], division_distribution: vec![10f64],
        volume_elasticity: [1.0, 1.5], active_exponents: [1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0],
        surface_tension: [1.0, 2.0, 3.0, 4.0, 5.0], modulation: [1.0, 1.0, 1.0], bead: hashset![],
        bead_parameters: [1.0, 1.0, 1.0],
        rng: mt19937::MT19937::default()
    };

    assert!(state.get_active_exponent(MEDIUM, EPI, false) == 1.0);
    assert!(state.get_active_exponent(MEDIUM, PRE, false) == 2.0);
    assert!(state.get_active_exponent(EPI, MEDIUM, false) == 3.0);
    assert!(state.get_active_exponent(EPI, EPI, false) == 4.0);
    assert!(state.get_active_exponent(EPI, PRE, false) == 5.0);
    assert!(state.get_active_exponent(PRE, MEDIUM, false) == 6.0);
    assert!(state.get_active_exponent(PRE, EPI, false) == 7.0);
    assert!(state.get_active_exponent(PRE, PRE, false) == 8.0);

    assert!(state.get_active_exponent(PRE, PRE, true) == 8.0);
    assert!(state.get_active_exponent(PRE, ECM, true) == f64::NEG_INFINITY);
    assert!(state.get_active_exponent(EPI, ECM, true) == f64::NEG_INFINITY);

    let kernel = Kernel::from_state(state);
    assert!(kernel.get_active_exponent(MEDIUM, 1, false) == 1.0);
    assert!(kernel.get_active_exponent(MEDIUM, 11, false) == 2.0);
    assert!(kernel.get_active_exponent(1, MEDIUM, false) == 3.0);
    assert!(kernel.get_active_exponent(2, 1, false) == 4.0);
    assert!(kernel.get_active_exponent(1, 12, false) == 5.0);
    assert!(kernel.get_active_exponent(11, MEDIUM, false) == 6.0);
    assert!(kernel.get_active_exponent(11, 2, false) == 7.0);
    assert!(kernel.get_active_exponent(11, 12, false) == 8.0);
}

/// Test in-place softmax function
#[test]
fn softmax() {
    let mut vec = vec![1f64, 1f64];
    softmax_(&mut vec);
    assert!((vec[0] - 0.5).abs() < 1e-12);
    assert!((vec[1] - 0.5).abs() < 1e-12);

    let mut vec = vec![1f64, 1e100f64];
    softmax_(&mut vec);
    assert!((vec[0] - 0.0).abs() < 1e-12);
    assert!((vec[1] - 1.0).abs() < 1e-12);

    let mut vec = vec![1f64, std::f64::MAX, std::f64::MAX / 2f64];
    softmax_(&mut vec);
    assert!((vec[0] - 0.0).abs() < 1e-12);
    assert!((vec[1] - 1.0).abs() < 1e-12);
    assert!((vec[2] - 0.0).abs() < 1e-12);

    let mut vec = vec![1f64, std::f64::MAX, std::f64::MIN];
    softmax_(&mut vec);
    assert!((vec[0] - 0.0).abs() < 1e-12);
    assert!((vec[1] - 1.0).abs() < 1e-12);
    assert!((vec[2] - 0.0).abs() < 1e-12);

    let mut vec = vec![1f64, std::f64::MAX, std::f64::MAX];
    softmax_(&mut vec);
    assert!((vec[0] - 0.0).abs() < 1e-12);
    assert!((vec[1] - 0.5).abs() < 1e-12);
    assert!((vec[2] - 0.5).abs() < 1e-12);

    let mut vec = vec![1f64, std::f64::MAX, std::f64::MAX - std::f64::EPSILON];
    softmax_(&mut vec);
    assert!((vec[0] - 0.0).abs() < 1e-12);
    assert!((vec[1] - 0.5).abs() < 1e-12);
    assert!((vec[2] - 0.5).abs() < 1e-12);
}

/// Test kernel data layout
#[test]
fn kernel_layout() {
    let state = State {
        side: 3, time: 0, cells: hashmap![
            1usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([13i64])
            },
            2usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([14i64, 23i64])
            },
            11usize => Cell {
                type_id: PRE, preferred_volume: 2f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([25i64, 26i64])
            },
            12usize => Cell {
                type_id: PRE, preferred_volume: 1f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([1i64])
            }
        ], ecm: hashset![], ecm_parameters: default_ecm_parameters(),
        action_probabilities: [0.001, 0.999, 0.999], death_probability: 0f64,
        growth_rate: [0.5, 0.5], division_distribution: vec![10f64],
        volume_elasticity: [1.0, 1.5], active_exponents: [0f64; 8],
        surface_tension: [1.0, 2.0, 3.0, 4.0, 5.0], modulation: [1.0, 1.0, 1.0], bead: hashset![],
        bead_parameters: [1.0, 1.0, 1.0],
        rng: mt19937::MT19937::default()
    };
    assert_eq!(state.side, 3);
    assert_eq!(state.cells.len(), 4);

    let mut kernel = Kernel::from_state(state);
    assert_eq!(kernel.state.side, 3);
    assert_eq!(kernel.side2, 9);
    assert_eq!(kernel.lattice.len(), 27);

    assert_eq!(kernel.id_to_index(0), [0, 0, 0]);
    assert_eq!(kernel.id_to_index(13), [1, 1, 1]);
    assert_eq!(kernel.id_to_index(14), [2, 1, 1]);
    assert_eq!(kernel.id_to_index(23), [2, 1, 2]);
    assert_eq!(kernel.id_to_index(25), [1, 2, 2]);
    assert_eq!(kernel.id_to_index(26), [2, 2, 2]);

    assert_eq!(kernel.index_to_id(0, 0, 0), 0);
    assert_eq!(kernel.index_to_id(1, 1, 1), 13);
    assert_eq!(kernel.index_to_id(2, 1, 1), 14);
    assert_eq!(kernel.index_to_id(2, 1, 2), 23);
    assert_eq!(kernel.index_to_id(1, 2, 2), 25);
    assert_eq!(kernel.index_to_id(2, 2, 2), 26);
    
    assert_eq!(kernel.index_to_id(0, 0, -1), BOUNDARY_VOXEL);
    assert_eq!(kernel.index_to_id(-2, 3, 1), BOUNDARY_VOXEL);
    assert_eq!(kernel.index_to_id(2, 2, 3), BOUNDARY_VOXEL);

    assert!(kernel.state.cells.contains_key(&1));
    assert!(kernel.state.cells.contains_key(&2));
    assert!(!kernel.state.cells.contains_key(&3));
    assert!(kernel.state.cells.contains_key(&11));
    assert!(kernel.state.cells.contains_key(&12));
    assert_eq!(kernel.state.cells.len(), 4);

    assert_eq!(kernel.state.cells[&1].get_volume(), 1);
    assert_eq!(kernel.state.cells[&2].get_volume(), 2);
    assert_eq!(kernel.state.cells[&11].get_volume(), 2);
    assert_eq!(kernel.state.cells[&12].get_volume(), 1);

    assert_eq!(kernel.get_value_type(0), MEDIUM);
    assert_eq!(kernel.get_value_type(1), EPI);
    assert_eq!(kernel.get_value_type(2), EPI);
    assert_eq!(kernel.get_value_type(11), PRE);
    assert_eq!(kernel.get_value_type(12), PRE);

    assert_eq!(kernel.get_surface_tension(0, 0), 0.);
    assert_eq!(kernel.get_surface_tension(1, 1), 0.);
    assert_eq!(kernel.get_surface_tension(2, 2), 0.);
    assert_eq!(kernel.get_surface_tension(11, 11), 0.);
    assert_eq!(kernel.get_surface_tension(12, 12), 0.);

    assert_eq!(kernel.get_surface_tension(0, 1), 1.);
    assert_eq!(kernel.get_surface_tension(1, 0), 1.);
    assert_eq!(kernel.get_surface_tension(0, 2), 1.);
    assert_eq!(kernel.get_surface_tension(2, 0), 1.);
    assert_eq!(kernel.get_surface_tension(0, 11), 2.);
    assert_eq!(kernel.get_surface_tension(11, 0), 2.);
    assert_eq!(kernel.get_surface_tension(0, 12), 2.);
    assert_eq!(kernel.get_surface_tension(12, 0), 2.);

    assert_eq!(kernel.get_surface_tension(1, 2), 3.);
    assert_eq!(kernel.get_surface_tension(2, 1), 3.);
    assert_eq!(kernel.get_surface_tension(1, 11), 4.);
    assert_eq!(kernel.get_surface_tension(11, 1), 4.);
    assert_eq!(kernel.get_surface_tension(2, 11), 4.);
    assert_eq!(kernel.get_surface_tension(11, 2), 4.);
    assert_eq!(kernel.get_surface_tension(2, 11), 4.);
    assert_eq!(kernel.get_surface_tension(11, 12), 5.);
    assert_eq!(kernel.get_surface_tension(12, 11), 5.);
    assert_eq!(kernel.get_surface_tension(1, 12), 4.);
    assert_eq!(kernel.get_surface_tension(12, 1), 4.);
    assert_eq!(kernel.get_surface_tension(2, 12), 4.);
    assert_eq!(kernel.get_surface_tension(12, 2), 4.);

    assert_eq!(kernel.calc_volume_penalty(1, 0f64), 0.5);
    assert_eq!(kernel.calc_volume_penalty(1, 1f64), 0.0);
    assert_eq!(kernel.calc_volume_penalty(2, 0f64), 0.0);
    assert_eq!(kernel.calc_volume_penalty(2, -1f64), 0.5);
    assert_eq!(kernel.calc_volume_penalty(2, 1f64), 0.5);
    assert_eq!(kernel.calc_volume_penalty(11, 0f64), 0.0);
    assert_eq!(kernel.calc_volume_penalty(11, -1f64), 0.75);
    assert_eq!(kernel.calc_volume_penalty(11, 1f64), 0.75);
    assert_eq!(kernel.calc_volume_penalty(12, 0f64), 0.0);
    assert_eq!(kernel.calc_volume_penalty(12, 1f64), 0.75);

    assert_eq!(kernel.get_index_value(0, 0, 0), 0);
    assert_eq!(kernel.get_index_value(1, 0, 0), 12);
    assert_eq!(kernel.get_index_value(1, 1, 1), 1);
    assert_eq!(kernel.get_index_value(2, 1, 1), 2);
    assert_eq!(kernel.get_index_value(2, 1, 2), 2);
    assert_eq!(kernel.get_index_value(1, 2, 2), 11);
    assert_eq!(kernel.get_index_value(2, 2, 2), 11);

    assert!(kernel.check_change(
        &kernel.get_voxel_by_id(kernel.index_to_id(0, 0, 0)), 0
    ));
    assert!(!kernel.check_change(
        &kernel.get_voxel_by_id(kernel.index_to_id(0, 0, 0)), 1
    ));
    assert!(!kernel.check_change(
        &kernel.get_voxel_by_id(kernel.index_to_id(1, 1, 1)), 1
    ));
    assert!(!kernel.check_change(
        &kernel.get_voxel_by_id(kernel.index_to_id(1, 1, 1)), 12
    ));
    assert!(kernel.check_change(
        &kernel.get_voxel_by_id(kernel.index_to_id(0, 2, 2)), 11
    ));

    kernel.propose_change(kernel.index_to_id(0, 1, 1));
    assert_eq!(kernel.changes.len(), 0);

    kernel.propose_change(kernel.index_to_id(1, 1, 1));
    assert_eq!(kernel.changes.len(), 1);
    let mut change = kernel.changes.pop().unwrap();
    assert_eq!(change.target_values.len(), 3);
    assert_eq!(change.possibilities.len(), 3);
    kernel.attempt_change(&mut change);
    assert_eq!(kernel.get_index_value(1, 1, 1), 1);

    assert_eq!(kernel.get_index_value(2, 1, 1), 2);
    kernel.propose_change(14i64);
    assert_eq!(kernel.changes.len(), 1);
    let mut change = kernel.changes.pop().unwrap();
    assert_eq!(change.target_values.len(), 3);
    assert_eq!(change.possibilities.len(), 3);
    kernel.attempt_change(&mut change);
    assert_eq!(kernel.get_index_value(2, 1, 1), 0);
}

/// Test cell division
#[test]
fn cell_division() {
    let vec1: [f64; DIM] = [1., 2., 3.];
    let vec2: [f64; DIM] = [3., 2., 1.];
    assert_eq!(vector_cross(&vec1, &vec2), [-4f64, 8f64, -4f64]);

    let tog:[f64; 2 * DIM] = [0.741596, 9.12836, 8.60687, -2.60184, 8.86378, -2.52642];
    assert!((calc_principle_value(&tog) - 18.4768).abs() < 5e-5);
    let vec = calc_principle_axis(&tog);
    assert!((vec[0] + 46.6803).abs() < 5e-5);
    assert!((vec[1] - 163.775).abs() < 5e-4);
    assert!((vec[2] - 159.028).abs() < 5e-4);

    let tog: [f64; 2 * DIM] = [
        1f64 + 5f64 * std::f64::EPSILON, 1f64 + 5f64 * std::f64::EPSILON,
        1f64 + 5f64 * std::f64::EPSILON, 5f64 * std::f64::EPSILON, 5f64 * std::f64::EPSILON,
        5f64 * std::f64::EPSILON
    ];
    assert!((calc_principle_value(&tog) - 1f64).abs() < 5e-5);
    let vec = calc_principle_axis(&tog);
    assert!((vec[0] - 1f64).abs() < 5e-5);
    assert!((vec[1] - 0f64).abs() < 5e-4);
    assert!((vec[2] - 0f64).abs() < 5e-4);

    let mut kernel = Kernel::from_state(State {
        side: 3, time: 0, cells: hashmap![
            2usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 2f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([14i64, 23i64])
            }
        ], ecm: hashset![], ecm_parameters: default_ecm_parameters(),
        action_probabilities: [0.001, 0.999, 0.999],
        death_probability: 0f64,
        growth_rate: [0.5, 0.5], division_distribution: vec![10f64],
        volume_elasticity: [1.0, 1.5], active_exponents: [0f64; 8],
        surface_tension: [1.0, 2.0, 3.0, 4.0, 5.0], modulation: [1.0, 1.0, 1.0], bead: hashset![],
        bead_parameters: [1.0, 1.0, 1.0],
        rng: mt19937::MT19937::default()
    });
    assert_eq!(kernel.state.cells.len(), 1);
    assert_eq!(kernel.next_id, 3usize);
    assert_eq!(kernel.state.cells[&2usize].get_volume(), 2usize);
    assert_eq!(kernel.lattice[14usize], 2);
    assert_eq!(kernel.lattice[23usize], 2);

    kernel.kill_and_divide();
    assert_eq!(kernel.state.cells.len(), 2);
    assert!(!kernel.state.cells.contains_key(&2usize));
    assert!(kernel.state.cells.contains_key(&3usize));
    assert!(kernel.state.cells.contains_key(&4usize));
    assert_eq!(kernel.state.cells[&3usize].parent_id, 2usize);
    assert_eq!(kernel.state.cells[&4usize].parent_id, 2usize);
    assert_eq!(kernel.state.cells[&3usize].age, 0usize);
    assert_eq!(kernel.state.cells[&4usize].age, 0usize);
    assert_eq!(kernel.state.cells[&3usize].type_id, EPI);
    assert_eq!(kernel.state.cells[&4usize].type_id, EPI);
    assert_eq!(kernel.state.cells[&3usize].preferred_volume, 1f64);
    assert_eq!(kernel.state.cells[&4usize].preferred_volume, 1f64);
    assert_eq!(kernel.state.cells[&3usize].get_volume(), 1usize);
    assert_eq!(kernel.state.cells[&4usize].get_volume(), 1usize);
    assert_eq!(kernel.state.cells[&3usize].division_volume, 10f64);
    assert_eq!(kernel.state.cells[&4usize].division_volume, 10f64);
    assert_eq!(kernel.lattice[14usize], 4);
    assert_eq!(kernel.lattice[23usize], 3);
    assert_eq!(kernel.next_id, 5usize);

    let mut kernel = Kernel::from_state(State {
        side: 3, time: 0, cells: hashmap![
            2usize => Cell {
                type_id: EPI, preferred_volume: 30f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from_iter(0..27)
            }
        ], ecm: hashset![], ecm_parameters: default_ecm_parameters(),
        action_probabilities: [0.001, 0.999, 0.999],
        death_probability: 0f64,
        growth_rate: [0.5, 0.5], division_distribution: vec![10f64],
        volume_elasticity: [1.0, 1.5], active_exponents: [0f64; 8], 
        surface_tension: [1.0, 2.0, 3.0, 4.0, 5.0], modulation: [1.0, 1.0, 1.0], bead: hashset![],
        bead_parameters: [1.0, 1.0, 1.0],
        rng: mt19937::MT19937::default()
    });
    assert_eq!(kernel.state.cells.len(), 1);
    kernel.kill_and_divide();
    assert_eq!(kernel.state.cells.len(), 2);
}

/// Test cell death
#[test]
fn cell_death() {
    let mut kernel = Kernel::from_state(State {
        side: 3, time: 0, cells: hashmap![
            2usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 2f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([14i64, 23i64])
            }
        ], ecm: hashset![], ecm_parameters: default_ecm_parameters(),
        action_probabilities: [0.001, 0.999, 0.999],
        death_probability: 100f64,
        growth_rate: [0.5, 0.5], division_distribution: vec![10f64],
        volume_elasticity: [1.0, 1.5], active_exponents: [0f64; 8],
        surface_tension: [1.0, 2.0, 3.0, 4.0, 5.0], modulation: [1.0, 1.0, 1.0], bead: hashset![],
        bead_parameters: [1.0, 1.0, 1.0],
        rng: mt19937::MT19937::default()
    });
    assert_eq!(kernel.state.cells.len(), 1);

    kernel.kill_and_divide();
    assert_eq!(kernel.state.cells.len(), 0);
    assert!(kernel.lattice.iter().all(|value| *value == 0));
}

/// Test gyration tensor
#[test]
fn tensor_gyration() {
    let kernel = Kernel::from_state(State {
        side: 3, time: 0, cells: hashmap![
            2usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 2f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([14i64, 23i64])
            }
        ], ecm: hashset![], ecm_parameters: default_ecm_parameters(),
        action_probabilities: [0.001, 0.999, 0.999],
        death_probability: 0f64,
        growth_rate: [0.5, 0.5], division_distribution: vec![10f64],
        volume_elasticity: [1.0, 1.5], active_exponents: [0f64; 8],
        surface_tension: [1.0, 2.0, 3.0, 4.0, 5.0], modulation: [1.0, 1.0, 1.0], bead: hashset![],
        bead_parameters: [1.0, 1.0, 1.0],
        rng: mt19937::MT19937::default()
    });
    let cell = &kernel.state.cells[&2usize];
    let com = kernel.get_cell_com(cell);
    assert_eq!(com[0], 2f64);
    assert_eq!(com[1], 1f64);
    assert_eq!(com[2], 1.5f64);

    let mut tog = [0f64; 2 * DIM];
    let mut counter = 0usize; // Counter for an accurate floating-point averaging
    for voxel_id in &cell.voxel_ids {
        counter += 1;
        let index = kernel.id_to_index(*voxel_id);
        let rcom = [
            index[0] as f64 - com[0], index[1] as f64 - com[1], index[2] as f64 - com[2]
        ];

        for i in 0..DIM {
            tog[i] += (rcom[i].powi(2) - tog[i]) / counter as f64;
            tog[DIM + i] += (rcom[i] * rcom[(i + 1) % DIM] - tog[DIM + i]) / counter as f64;
        }
    }
    assert_eq!(tog[0], 0f64);
    assert_eq!(tog[1], 0f64);
    assert_eq!(tog[2], 0.25f64);
    assert_eq!(tog[3], 0f64);
    assert_eq!(tog[4], 0f64);
    assert_eq!(tog[5], 0f64);
}

/// Test gyration tensor
#[test]
fn principle_axis() {
    let tog = [
        0.6666666666666665, 0.6666666666666664, 0.6666666666666665,
        -8.22387425648264e-18, -1.3877787807814457e-17, 1.5419764230904959e-18
    ];
    let pval = calc_principle_value(&tog);
    assert!(!pval.is_nan());
    let axis = calc_principle_axis(&tog);
    assert!(!axis[0].is_nan());
    assert!(!axis[1].is_nan());
    assert!(!axis[2].is_nan());
}

/// Test connectivity check
#[test]
fn connectivity_check() {
    let kernel = Kernel::from_state(State {
        side: 3, time: 0, cells: hashmap![
            2usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 2f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([0i64, 1i64, 26i64])
            }
        ], ecm: hashset![], ecm_parameters: default_ecm_parameters(),
        action_probabilities: [0.001, 0.999, 0.999],
        death_probability: 0f64, growth_rate: [0.5, 0.5], division_distribution: vec![10f64],
        volume_elasticity: [1.0, 1.5], active_exponents: [0f64; 8],
        surface_tension: [1.0, 2.0, 3.0, 4.0, 5.0], modulation: [1.0, 1.0, 1.0], bead: hashset![],
        bead_parameters: [1.0, 1.0, 1.0],
        rng: mt19937::MT19937::default()
    });

    assert!(!kernel.check_connectivity(&kernel.state.cells[&2]));
}

/// Test planar index
#[test]
fn planar_index() {
    let mut kernel = Kernel::from_state(State {
        side: 3, time: 0, cells: hashmap![
            1usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 2f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: hashset!{} // std::collections::HashSet::from([4i64, 10i64, 12i64, 14i64, 16i64, 22i64])
            }
        ], ecm: hashset![], ecm_parameters: default_ecm_parameters(),
        action_probabilities: [0.001, 0.999, 0.999],
        death_probability: 0f64,
        growth_rate: [0.5, 0.5], division_distribution: vec![10f64],
        volume_elasticity: [1.0, 1.5], active_exponents: [0f64; 8],
        surface_tension: [1.0, 2.0, 3.0, 4.0, 5.0], modulation: [1.0, 1.0, 1.0], bead: hashset![],
        bead_parameters: [1.0, 1.0, 1.0],
        rng: mt19937::MT19937::default()
    });
    let voxel = kernel.get_voxel_by_id(13i64);

    // Case 1 (0)
    kernel.lattice.copy_from_slice(&[0; 27]);
    assert_eq!(kernel.planar_index(&voxel, 1), [0, 0, 0]);

    // Case 2 (1)
    kernel.lattice.copy_from_slice(&[0; 27]);
    kernel.lattice[12] = 1;
    assert_eq!(kernel.planar_index(&voxel, 1), [0, 1, 1]);

    // Case 3 (2)
    kernel.lattice.copy_from_slice(&[0; 27]);
    kernel.lattice[10] = 1;
    kernel.lattice[16] = 1;
    assert_eq!(kernel.planar_index(&voxel, 1), [0, 2, 2]);

    // Case 4 (2)
    kernel.lattice.copy_from_slice(&[0; 27]);
    kernel.lattice[12] = 1;
    kernel.lattice[16] = 1;
    assert_eq!(kernel.planar_index(&voxel, 1), [1, 1, 2]);

    // Case 5 (3)
    kernel.lattice.copy_from_slice(&[0; 27]);
    kernel.lattice[10] = 1;
    kernel.lattice[12] = 1;
    kernel.lattice[16] = 1;
    assert_eq!(kernel.planar_index(&voxel, 1), [1, 2, 3]);

    // Case 6 (3)
    kernel.lattice.copy_from_slice(&[0; 27]);
    kernel.lattice[12] = 1;
    kernel.lattice[16] = 1;
    kernel.lattice[22] = 1;
    assert_eq!(kernel.planar_index(&voxel, 1), [2, 2, 2]);

    // Case 7 (4)
    kernel.lattice.copy_from_slice(&[0; 27]);
    kernel.lattice[10] = 1;
    kernel.lattice[12] = 1;
    kernel.lattice[16] = 1;
    kernel.lattice[22] = 1;
    assert_eq!(kernel.planar_index(&voxel, 1), [2, 3, 3]);

    // Case 8 (4)
    kernel.lattice.copy_from_slice(&[0; 27]);
    kernel.lattice[10] = 1;
    kernel.lattice[12] = 1;
    kernel.lattice[14] = 1;
    kernel.lattice[16] = 1;
    assert_eq!(kernel.planar_index(&voxel, 1), [2, 2, 4]);

    // Case 9 (5)
    kernel.lattice.copy_from_slice(&[0; 27]);
    kernel.lattice[10] = 1;
    kernel.lattice[12] = 1;
    kernel.lattice[14] = 1;
    kernel.lattice[16] = 1;
    kernel.lattice[22] = 1;
    assert_eq!(kernel.planar_index(&voxel, 1), [3, 3, 4]);

    // Case 10 (6)
    kernel.lattice.copy_from_slice(&[0; 27]);
    kernel.lattice[4] = 1;
    kernel.lattice[10] = 1;
    kernel.lattice[12] = 1;
    kernel.lattice[14] = 1;
    kernel.lattice[16] = 1;
    kernel.lattice[22] = 1;
    assert_eq!(kernel.planar_index(&voxel, 1), [4, 4, 4]);
}

/// Test accessory getters
#[test]
fn kernel_getters() {
    let kernel = Kernel::from_state(State {
        side: 3, time: 0, cells: hashmap![
           5usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([13i64])
            },
            7usize => Cell {
                type_id: EPI, preferred_volume: 2f64, division_volume: 10f64,
                parent_id: 0usize, age: 0usize,
                voxel_ids: std::collections::HashSet::from([14i64, 23i64])
            }
        ], ecm: hashset![], ecm_parameters: default_ecm_parameters(),
        action_probabilities: [0.001, 0.999, 0.999],
        death_probability: 0f64,
        growth_rate: [0.5, 0.5], division_distribution: vec![10f64],
        volume_elasticity: [1.0, 1.5], active_exponents: [0f64; 8],
        surface_tension: [1.0, 2.0, 3.0, 4.0, 5.0], modulation: [1.0, 1.0, 1.0], bead: hashset![],
        bead_parameters: [1.0, 1.0, 1.0],
        rng: mt19937::MT19937::default()
    });

    assert_eq!(kernel.contact_between(5, 7), 1);
    assert_eq!(kernel.cell_area(5), 6);
    assert_eq!(kernel.cell_area(7), 10);
    assert_eq!(kernel.cell_ii(5), 6f64);
    assert_eq!(kernel.cell_ii(7), 10 as f64 / 2 as f64);
    assert_eq!(kernel.cell_gyradius(5), 0f64);
    assert_eq!(kernel.cell_gyradius(7), 0.5f64);
}